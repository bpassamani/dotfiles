# setting some default stuff
export TERMINAL="alacritty"
export EDITOR="nvim"
export BROWSER="firefox"
export READER="zathura"
export VISUAL="nvim"
export VIDEO="mpv"
export IMAGE="sxiv"
export OPENER="xdg-open"
export PAGER="less"
export WM="dwm"

# setting up PATHs
export PATH="$HOME/.config/scripts:$PATH"
export PATH="$HOME/.config/scripts/dwm_blocks:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.local/share/dwm:$PATH"
export PATH="$HOME/.local/bin/statusbar:$PATH"

# XGD paths
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}

# fixing PATHs
export ZDOTDIR=$HOME/.config/zsh
