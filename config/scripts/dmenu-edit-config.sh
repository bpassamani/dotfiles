#!/bin/bash
# Dmenu script for editing some of my more frequently edited config files.


declare options=("alacritty
bash
bspwm
deadd
doom.d/config.el
doom.d/init.el
doom.d/packages.el
emacs.d/init.el
neovim
picom
polybar
ranger
sxhkd
vim
xmobar
xmonad
zsh
quit")

choice=$(echo -e "${options[@]}" | dmenu -h 27 -p 'config: ')

case "$choice" in
	quit)
		echo "Program terminated." && exit 1
	;;
	alacritty)
		choice="$HOME/.config/alacritty/alacritty.yml"
	;;
	bash)
		choice="$HOME/.bashrc"
	;;
	deadd)
		choice="$HOME/.config/deadd/deadd.conf"
	;;
	bspwm)	
		choice="$HOME/.config/bspwm/bspwmrc"
	;;
    doom.d/config.el)
		choice="$HOME/.doom.d/config.el"
	;;
    doom.d/init.el)
		choice="$HOME/.doom.d/init.el"
	;;
	doom.d/packages.el)
		choice="$HOME/.doom.d/packages.el"
	;;
	emacs.d/init.el)
		choice="$HOME/.emacs.d/init.el"
	;;
	neovim)
		choice="$HOME/.config/nvim/init.vim"
	;;
	picom)
		choice="$HOME/.config/picom/picom.conf"
	;;
	polybar)
		choice="$HOME/.config/polybar/config"
	;;
	ranger)
		choice="$HOME/.config/ranger/rc.conf"
	;;
	sxhkd)
		choice="$HOME/.config/sxhkd/sxhkdrc"
	;;
	vim)
		choice="$HOME/.vimrc"
	;;
	xmobar)
		choice="$HOME/.config/xmobar/xmobar.hs"
	;;
	xmonad)
		choice="$HOME/.xmonad/xmonad.hs"
	;;
	zsh)
		choice="$HOME/.zshrc"
	;;
	*)
		exit 1
	;;
esac
alacritty -e vim "$choice"
